#pragma once

#include <iostream>
#include "ScreenManager.h"
#include "InputManager.h"

class GameScreen
{
public:
	GameScreen();
	~GameScreen();

	virtual void LoadContent();
	virtual void UnloadContent();
	virtual void HandleInput(sf::Event event);
	virtual void Update();
	virtual void Draw(sf::RenderWindow &Window);

protected:
	std::vector<sf::Keyboard::Key> keys;
};

