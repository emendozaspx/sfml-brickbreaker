#include "InputManager.h"



InputManager::InputManager()
{
}


InputManager::~InputManager()
{
}

void InputManager::Update(sf::Event event)
{
	this->event = event;
}

bool InputManager::KeyPressed(sf::Keyboard::Key key)
{
	if (event.key.code == key && event.KeyPressed) {
		return true;
	}
	return false;
}

bool InputManager::KeyPressed(std::vector<sf::Keyboard::Key> keys)
{
	for (std::size_t i = 0; i < keys.size(); i++) {
		if (event.key.code == keys[i] && event.KeyPressed) {
			return true;
		}
		return false;
	}
}

bool InputManager::KeyReleased(sf::Keyboard::Key key)
{
	if (event.key.code == key && event.KeyReleased) {
		return true;
	}
	return false;
}

bool InputManager::KeyReleased(std::vector<sf::Keyboard::Key> keys)
{
	for (std::size_t i = 0; i < keys.size(); i++) {
		if (event.key.code == keys[i] && event.KeyReleased) {
			return true;
		}
		return false;
	}
}