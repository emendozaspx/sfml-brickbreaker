#pragma once

#include <vector>
#include <SFML/Window.hpp>

class InputManager
{
public:
	InputManager();
	~InputManager();

	void Update(sf::Event event);
	
	bool KeyPressed(sf::Keyboard::Key key);
	bool KeyPressed(std::vector<sf::Keyboard::Key> keys);

	bool KeyReleased(sf::Keyboard::Key key);
	bool KeyReleased(std::vector<sf::Keyboard::Key> keys);

private:
	sf::Event event;
};

