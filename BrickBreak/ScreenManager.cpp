#include "ScreenManager.h"



ScreenManager::ScreenManager()
{
}


ScreenManager::~ScreenManager()
{
}

ScreenManager &ScreenManager::GetInstance()
{
	static ScreenManager instance;
	return instance;
}

void ScreenManager::Initialize()
{

}

void ScreenManager::LoadContent()
{

}

void ScreenManager::UnloadContent()
{

}

void ScreenManager::HandleInput(sf::Event event)
{

}

void ScreenManager::Update()
{

}

void ScreenManager::Draw(sf::RenderWindow &Window)
{

}