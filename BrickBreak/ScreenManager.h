#pragma once

#include <SFML/Graphics.hpp>

class ScreenManager
{
public:
	~ScreenManager();
	static ScreenManager &GetInstance();

	void Initialize();
	void LoadContent();
	void UnloadContent();
	void HandleInput(sf::Event event);
	void Update();
	void Draw(sf::RenderWindow &Window);

private:
	ScreenManager();
	ScreenManager(ScreenManager const&);
	void operator=(ScreenManager const&);
};

