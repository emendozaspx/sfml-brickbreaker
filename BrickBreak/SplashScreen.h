#pragma once

#include "GameScreen.h"

class SplashScreen : public GameScreen
{
public:
	SplashScreen();
	~SplashScreen();

	void LoadContent();
	void UnloadContent();
	void HandleInput(sf::Event event);
	void Update();
	void Draw(sf::RenderWindow &Window);
};

