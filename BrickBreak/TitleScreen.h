#pragma once
#include "GameScreen.h"
class TitleScreen :
	public GameScreen
{
public:
	TitleScreen();
	~TitleScreen();

	void LoadContent();
	void UnloadContent();
	void HandleInput(sf::Event event);
	void Update();
	void Draw(sf::RenderWindow &Window);
};

